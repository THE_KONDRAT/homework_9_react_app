import { Component, MouseEventHandler } from 'react';
import axios from 'axios';
import { Login } from './LoginComponent';
import { User } from './LoginComponent';

export interface IUserExampleComponentProps{
    onClick?: (user: User)=>void;
}

interface ISingleUserExampleComponentProps{
    onClick?: (user: User)=>void;
    firstName: string;
    lastName: string;
    username: string;
    password: string;
}

export default class UsersExampleComponent extends Component<IUserExampleComponentProps>{
    info: Array<{id:number, firstName: string, lastName: string, username: string, password: string}> = [];

    constructor(props: any){
        super(props);
    }

    tableHeadStyle={borderStyle: "solid", borderColor: "Gray", borderWidth: "0 0 0.1rem 0", padding: "0" }
    showUsersCount = 5;
    componentDidMount() {
        axios({
            method: 'get',
            url: `/users?limit=${this.showUsersCount}&skip=${Math.floor(Math.random() * (100 - this.showUsersCount))}&select=firstName,lastName,username,password&q=proxy`,
            withCredentials: false
        })
        .then((response) => { this.info = response.data.users; this.forceUpdate();})
        .catch((error) => { this.info = []; console.log(error); this.forceUpdate(); });
    };

    //omnomnom = (username:string, password:string) => { console.log(`Login: ${username}; password: ${password}`)};

    render(){
        const buttonStyle={
            borderStyle: "solid",
            borderColor: "Gray",
            borderWidth: "0.4rem",
            borderRadius: "0.8rem",
            backgroundColor: "DimGray",
            padding: "0.1rem 0.75rem 0.2rem 0.75rem",
            fontWeight: "800",
            color: "GhostWhite"
        };
        return(
            <div>
                <h4>Примеры учётных данных</h4>
                {this.info.length > 0 &&
                    <table style={{borderStyle: "solid", borderColor: "Gray", borderWidth: "0.4rem 0 0.4rem 0", padding: "0 0.75rem 0.45rem 0.75rem"}}>
                        <thead>
                            <tr>
                                <td style={this.tableHeadStyle}></td>
                                <td style={this.tableHeadStyle}>Имя</td>
                                <td style={this.tableHeadStyle}>Фамилия</td>
                                <td style={this.tableHeadStyle}>Логин</td>
                                <td style={this.tableHeadStyle}>Пароль</td>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                
                                this.info.map((item, i) =>
                                    <SingleUserComponent  onClick={this.props.onClick} firstName={item.firstName} lastName={item.lastName} username={item.username} password={item.password} key={i} />
                                )
                            }
                        </tbody>
                    </table>
                }
                {this.info.length == 0 &&
                    <span>...</span>
                }
            </div>
        );
    }
}

function SingleUserComponent(props: ISingleUserExampleComponentProps, key: number){
    const name = '';
    return(
        <tr className='UserExample' key={key}>
            <td>
                <button onClick={() => {Login(props.username, props.password, props.onClick)}}>Войти</button>
            </td>
            <td>{props.firstName}</td>
            <td>{props.lastName}</td>
            <td>{props.username}</td>
            <td>{props.password}</td>
        </tr>
    );
}